package main

import "fmt"

func main() {
	const name string = "Constant name"

	fmt.Println(name)

	// assign constant to other variables
	const otherName = name
	fmt.Println(otherName)

	// trying to changing name variable,
	// should be an error
	//name = "name"

	// create a constant variable without telling the type
	const myFriendName = "Joie"
	fmt.Println(myFriendName)

}
