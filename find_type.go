package main

import (
	"fmt"
	"reflect"
)

func main() {

	var name string = "Joie"
	var count int

	count = 10

	fmt.Printf("type of name = %T\n", name)
	fmt.Printf("type of count = %T\n", count)

	// with reflection
	fmt.Println("type of name = ", reflect.TypeOf(name).String())
	fmt.Println("type of count = ", reflect.TypeOf(count).String())
}
