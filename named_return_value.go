package main

import "fmt"

//return values may be named. If so, they are treated as variables defined
// at te top of the function
func swap(a, b int) (y, x int) {
	y = b
	x = a
	return
}

func main() {
	fmt.Println(swap(10, 5))
}
