package main

import "fmt"

func main() {

	// a defer statement defers the execution of a function until the surrounding
	// function returns

	// The deferred call's arguments are evaluted immediately, but the function call
	// is not executed until the surrounding function returns.

	defer fmt.Println("Satu")
	fmt.Println("Dua")
	fmt.Println("Tiga")
}
