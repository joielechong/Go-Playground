package main

import "fmt"

func swap(a, b int) (int, int) {
	return b, a
}

func swapName(firstName, lastName string) (string, string) {
	return lastName, firstName
}

func main() {
	fmt.Println(swap(10, 5))
	fmt.Println(swapName("Juan", "rio"))
}
