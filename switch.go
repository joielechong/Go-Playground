package main

import (
	"fmt"
	"runtime"
)

func main() {
	//darwin, freebsd, linux,
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("darwin")
	case "linux":
		fmt.Println("Linux rock!")
	default:
		fmt.Println(os)
	}
}
