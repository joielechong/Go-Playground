package main

import (
	"fmt"
	s "strings"
	"unicode"
)

var f = fmt.Printf

func main() {
	upper := s.ToUpper("Hello there!")
	f("To upper: %s\n", upper)
	f("To Lower: %s\n", s.ToLower("Hello THERE"))

	f("%s\n", s.Title("tHis wiLL be A title!"))

	f("EqualFold: %v\n", s.EqualFold("Joielechong", "JOIElechong"))
	f("EqualFold: %v\n", s.EqualFold("Joielechong", "JOIELecho"))

	f("Prefix: %v\n", s.HasPrefix("Joielechong", "Jo"))
	f("Prefix: %v\n", s.HasPrefix("Joielechong", "jo"))
	f("Suffix: %v\n", s.HasSuffix("Joielechong", "ng"))
	f("Suffix: %v\n", s.HasSuffix("Joielechong", "NG"))

	f("Index: %v\n", s.Index("Joielechong", "le"))
	f("Index: %v\n", s.Index("Joielechong", "Le"))
	f("Count: %v\n", s.Count("Joielechong", "e"))
	f("count: %v\n", s.Count("Joielechong", "E"))
	f("repeat: %s\n", s.Repeat("ab", 5))

	f("TrimSpace: %s\n", s.TrimSpace(" \tThis is a line. \n"))
	f("TrimLeft: %s", s.TrimLeft(" \tTis is a\t line. \n", "\n\t"))
	f("TrimRight: %s\n", s.TrimRight(" \tThis is a\t line. \n", "\n\t "))

	f("Compare: %v\n", s.Compare("Joielechong", "JOIELECHONG"))
	f("Compare: %v\n", s.Compare("Joielechong", "Joielechong"))
	f("Compare: %v\n", s.Compare("JOIELECHONG", "JOIelechong"))

	f("Fields: %v\n", s.Fields("this is a string!"))
	f("Fields: %v\n", s.Fields("thisis\na\tstring!"))

	f("%s\n", s.Split("abcd efg", ""))

	f("%s\n", s.Replace("abcd efg", "", "_", -1))
	f("%s\n", s.Replace("abcd efg", "", "_", 4))
	f("%s\n", s.Replace("abcd efg", "", "_", 2))

	lines := []string{"Line 1", "Line 2", "Line 3"}
	f("Join: %s\n", s.Join(lines, "++++"))

	f("SplitAfter: %s\n", s.SplitAfter("123++432++", "++"))

	trimFunction := func(c rune) bool {
		return !unicode.IsLetter(c)
	}

	f("TrimFunc: %s\n", s.TrimFunc("123 abc ABC \t .", trimFunction))
}
