package main

import "fmt"

func printString(from string) {
	for i := 0; i < 5; i++ {
		fmt.Println(from, ":", i)
	}
}

func main() {
	printString("direct")

	go printString("goroutine")

	go func(message string) {
		fmt.Println(message)
	}("Another goroutine")
	fmt.Scanln()
	fmt.Println("Finish")
}
