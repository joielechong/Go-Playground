package main

import (
	"io"
	"os"
)

func main() {
	hello := "Hello"
	world := "world"

	io.WriteString(os.Stdout, hello)
	io.WriteString(os.Stdout, world)
}
