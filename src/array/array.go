package main

import (
	"fmt"
	"strconv"
)

func main() {

	var names [3]string
	fmt.Println("empty array = ", names)

	names[2] = "Test"
	fmt.Println("names = ", names)
	fmt.Println("names[2] = ", names[2])

	fmt.Println("names length = ", len(names))

	for i := 0; i < 3; i++ {
		names[i] = "String " + strconv.Itoa(i)
	}

	fmt.Println(names)
}
