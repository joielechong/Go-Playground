package main

// check the node trees with the following command:
//go tool compile -W nodetreemore.go
import "fmt"

func functionOne(x int) {
	fmt.Println(x)
}

func main() {
	varOne := 1
	varTwo := 2
	fmt.Println("Hello there!")
	functionOne(varOne)
	functionOne(varTwo)
}
