package main

import "fmt"

type User struct {
	firstName, lastName string
}

func printDetails(u *User) {
	fmt.Println(u.firstName + " " + u.lastName)
}

func main() {
	u := User{"Joie", "Le Chong"}

	printDetails(&u)
}
