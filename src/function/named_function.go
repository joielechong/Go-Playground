package main

import "fmt"

func location(name, city string) (region, continent string) {
	switch city {
	case "Medan", "Binjai", "Besitang":
		continent = "Asia"
	default:
		continent = "Unknown"
	}

	return
}

func main() {
	region, continent := location("Siantar", "Sintar")
	fmt.Printf("%s in continent %s\n", region, continent)
}
