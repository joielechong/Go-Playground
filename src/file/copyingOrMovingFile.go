package main

import (
	"io"
	"os"
)

func main() {
	original, err := os.Open("original.txt")

	if err != nil {
		panic(err)
	}

	defer original.Close()

	originalCopy, err2 := os.Create("copy.txt")
	if err2 != nil {
		panic(err2)
	}

	defer originalCopy.Close()

	_, err3 := io.Copy(originalCopy, original)

	if err3 != nil {
		panic(err3)
	}
}
