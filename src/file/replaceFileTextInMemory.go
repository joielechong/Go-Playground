// Write a Go program that takes three arguments,
// the name of a text file, and two strings.
// This utility should then replace every occurrence
// of the first string in the file with the second string.
// For reasons of security, the final output will be printed
// on screen, which means that the original text file will
// remain intact.

package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {

	args := os.Args
	if len(args) != 4 {
		fmt.Println("<File> <string> <string>")
		return
	}

	file := os.Args[1]
	f, err := os.Open(file)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()

	str1 := os.Args[2]
	str2 := os.Args[3]

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		str := scanner.Text()
		str = strings.Replace(str, str1, str2, -1)
		io.WriteString(os.Stdout, str)
		io.WriteString(os.Stdout, "\n")
	}
}
