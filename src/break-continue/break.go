package main

import "fmt"

func main() {
	power := make([]int, 10)

	for i := range power {
		power[i] = 1 << uint(i)
		if power[i] >= 16 {
			break
		}
	}

	fmt.Println(power)
}
