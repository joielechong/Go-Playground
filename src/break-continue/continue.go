package main

import "fmt"

func main() {
	power := make([]int, 10)

	for i := range power {
		if i%2 == 0 {
			continue
		}
		power[i] = 1 << uint(i)
	}

	fmt.Println(power)
}
