// A Map maps keys to values.
// The zero value of a map is nil.
// A nil map has no keys, nor can keys be added.
// The make function returs a map of the give type, initialized and ready for use

package main

import "fmt"

type Vertex struct {
	Lat, Long float64
}

var m map[string]Vertex

func main() {
	m = make(map[string]Vertex)
	m["Bell Labs"] = Vertex{
		40.68433, -74.39967,
	}
	fmt.Println(m["Bell Labs"])

	v1 := Vertex{2, 4}
	m["val"] = v1

	fmt.Println(m["val"])
}
