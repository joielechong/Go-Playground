package main

import "fmt"

func main() {

	mangas := make(map[string]int)

	mangas["One Piece"] = 1

	fmt.Printf("Manga One Piece is number %d\n", mangas["One Piece"])

	mangas["One Piece"] = 0

	fmt.Printf("Manga One Piece m[\"One Piece\"] is number %d\n", mangas["One Piece"])

	mangas["Naruto"] = 2
	fmt.Printf("Adding Naruto as number %d\n", mangas["Naruto"])

	mangas["Conan"] = 3
	fmt.Printf("Adding Conan as number %d\n", mangas["Conan"])

	fmt.Println("Deleting Naruto..")
	delete(mangas, "Naruto")

	fmt.Printf("trying to read Naruto = %v\n", mangas["Naruto"])
	_, ok := mangas["Naruto"]
	fmt.Printf("is Naruto still exist = %t\n", ok)
}
