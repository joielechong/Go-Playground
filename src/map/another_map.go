package main

import "fmt"

func main() {
	m := make(map[string]int)

	m["key1"] = 1
	m["key2"] = 3

	fmt.Println("map:", m)

	fmt.Println("val1:", m["key1"])
	fmt.Println("val2:", m["key2"])

	fmt.Println("len:", len(m))

	fmt.Println("Deleting a value")

	delete(m, "key1")
	fmt.Println("map:", m)

	_, isPresent := m["key1"]
	fmt.Println("is key1 value present:", isPresent)
}
