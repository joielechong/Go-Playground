package main

import "fmt"

func main() {
	cities := map[string]int{
		"Besitang":   12345,
		"Medan":      100000,
		"Perbaungan": 12347,
	}

	for key, value := range cities {
		fmt.Printf("%s has %d inhabitants\n", key, value)
	}
}
