package main

import "fmt"

type Vertex struct {
	Lat, Long float64
}

var m = map[string]Vertex{
	"Bell Labs": {40.68433, -74.39967},
	"Google":    {37.42202, -122.08408},
}

type Vehicle struct {
	x, y int
}

var n = map[string]Vehicle{
	"a": {1, 2},
	"b": {3, 4},
}

func main() {
	fmt.Println(m)
	fmt.Println(n)
}
