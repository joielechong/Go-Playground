package main

import "fmt"

func main() {
	power := make([]int, 10)

	for i := range power {
		power[i] = 1 << uint(i)
	}

	fmt.Println(power)

	for i, _ := range power {
		fmt.Print(i, " ")
	}

	fmt.Println()

	for _, val := range power {
		fmt.Print(val, " ")
	}

	fmt.Println()
}
