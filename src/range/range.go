package main

import "fmt"

func main() {
	nums := []int{2, 3, 4}

	// Range result is in index, value

	for index, value := range nums {
		fmt.Print("Index:", index)
		fmt.Println(" Value:", value)

	}

	// we can omit the index using underscore
	sum := 0
	for _, value := range nums {
		sum += value
	}

	fmt.Println("sum is ", sum)

	fmt.Println("Range can also be used for map")
	fruits := map[string]string{"a": "apple", "b": "banana"}
	fmt.Println("fruits: ", fruits)
	for key, value := range fruits {
		fmt.Printf("key:%s, value:%s\n", key, value)
	}

	fmt.Println("Range can also iterate over just the keys of map")
	for key := range fruits {
		fmt.Println("key:", key)
	}

	fmt.Println("Range can also iterate over unicode value")
	for index, char := range "golang" {
		fmt.Printf("index: %d, char: %d\n", index, char)
	}
}
