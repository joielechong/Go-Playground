package main

import (
	"fmt"

	"github.com/andlabs/ui"
)

var buttons [9]*ui.Button
var buttonIds map[*ui.Button]int

func main() {

	err := ui.Main(func() {

		buttonIds = make(map[*ui.Button]int)

		// Create the row
		var horBoxs [3]*ui.Box
		for i := 0; i < 3; i++ {
			horBoxs[i] = ui.NewHorizontalBox()
		}

		for i := 0; i < 9; i++ {
			// Create the button
			buttons[i] = ui.NewButton("X")
			buttons[i].OnClicked(OnClicked)
			buttonIds[buttons[i]] = i

			// then attach them to the row.
			switch {
			case i < 3:
				horBoxs[0].Append(buttons[i], true)
			case i < 6:
				horBoxs[1].Append(buttons[i], true)
			case i < 9:
				horBoxs[2].Append(buttons[i], true)
			}
		}

		// Attach to root layout
		box := ui.NewVerticalBox()
		for i := 0; i < 3; i++ {
			box.Append(horBoxs[i], true)
		}

		window := ui.NewWindow("Tic Tac Toe", 500, 500, false)
		window.SetMargined(true)
		window.SetChild(box)
		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})
		window.Show()
	})
	if err != nil {
		panic(err)
	}
}

func OnClicked(button *ui.Button) {
	fmt.Println(buttonIds[button])
	fmt.Println("clicked")
	if button.Text() == "X" {
		button.SetText("O")
	}
}
