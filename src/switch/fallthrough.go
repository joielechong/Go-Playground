package main

import "fmt"

func main() {
	n := 7

	switch n {
	case 0, 1, 2, 3, 4:
		fmt.Println("Very bad!")
	case 5, 6:
		fmt.Println("Mediocre")
	case 7:
		fmt.Println("Not bad")
		fallthrough
	case 8:
		fmt.Println("Good")
		fallthrough
	case 9:
		fmt.Println("Great!")
		fallthrough
	case 10:
		fmt.Println("Excellent")
	default:
		fmt.Println("What??")
	}
}
