package main

import "fmt"

var (
	totalCoin = 100
	users     = []string{
		"Joielechong", "MaFe", "Zonic", "Augus",
		"Juan Khan", "Bohalima", "Fiscok",
		"HarjoshMes", "Korintus", "Vernando",
	}
	coinForUsers = make(map[string]int, len(users))
)

func main() {

	for _, name := range users {

		var coins int
		for _, runeVal := range name {

			switch runeVal {
			case 'a':
				coins += 1
			case 'e':
				coins += 1
			case 'i':
				coins += 2
			case 'o':
				coins += 3
			case 'u':
				coins += 4
			}
		}
		coinForUsers[name] = coins
		totalCoin -= coins
	}

	fmt.Println(coinForUsers)
	fmt.Println("Coins left:", totalCoin)
}
