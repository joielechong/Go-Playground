package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("Please give one or more numeric values")
		os.Exit(1)
	}

	var total int64
	arguments := os.Args
	k := 0
	for i := 1; i <= len(arguments); i++ {
		num, err := strconv.ParseInt(arguments[k], 10, 64)
		if err == nil {
			total = total + num
		} else {
			k++
		}
	}

	if k >= len(arguments) {
		fmt.Println("None the arguments is numeric!")
		return
	}

	fmt.Println("total :", total)

}
