package main

import "fmt"

type Cube struct {
	S float64
}

func (c *Cube) Area() float64 {
	return c.S * c.S * 6
}

func (c *Cube) Perimeter() float64 {
	return 12 * c.S
}

func (c *Cube) Volume() float64 {
	return c.S * c.S * c.S
}
func main() {

	cube := Cube{3}

	fmt.Println("Cube area: ", cube.Area())
	fmt.Println("cube perimeter: ", cube.Perimeter())
	fmt.Println("Cube volume: ", cube.Volume())
}
