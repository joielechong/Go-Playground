package main

import (
	"fmt"
)

type testiface interface {
	SayHello()
	Say(s string)
	Increment()
	GetInternalValue() int
}

type testConcretImpl struct {
	i int
}

func NewTestConcreteImpl() testiface {
	return new(testConcretImpl)
}

func NewTestConcreteWithValueImpl(val int) testiface {
	return &testConcretImpl{i: val}
}

func (tst *testConcretImpl) SayHello() {
	fmt.Println("Hello")
}

func (tst *testConcretImpl) Say(s string) {
	fmt.Println(s)
}

func (tst *testConcretImpl) Increment() {
	tst.i++
}

func (tst *testConcretImpl) GetInternalValue() int {
	return tst.i
}

// we want this struct to have all the features of *testConcreteImpl
type testEmbedding struct {
	*testConcretImpl // embedding

}

func testEmptyInterface(v interface{}) {
	//	if i, ok := v.(int); ok {
	//		fmt.Println("I'm an integer", i)
	//	}

	switch v.(type) {
	case int:
		fmt.Println("I'm an integer", v)
	case string:
		fmt.Println("I'm a string", v)
	default:
		fmt.Println("I'm alien", v)
	}
}

func main() {
	var tiface testiface
	tiface = NewTestConcreteWithValueImpl(10)

	tiface.SayHello()
	tiface.Say("Hello again!!")

	tiface.Increment()
	tiface.Increment()
	tiface.Increment()
	fmt.Println(tiface.GetInternalValue())

	te := testEmbedding{testConcretImpl: &testConcretImpl{i: 10}}
	te.SayHello()
	te.Increment()
	te.GetInternalValue()
	fmt.Println(te.GetInternalValue())

	testEmptyInterface(7)
	testEmptyInterface("This is string")
	testEmptyInterface(te)
}
