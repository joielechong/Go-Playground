package main

import "fmt"

// a type assertion provide access to an interfaace value's underlying concrete value.
// t := i.(T)

func main() {
	var i interface{} = "Hello"

	s := i.(string)
	fmt.Println(s)

	s, ok := i.(string)
	fmt.Println(s, ok)

	f, ok := i.(float64)
	fmt.Println(f, ok)
}
