package main

import "fmt"

type User struct {
	FirstName, LastName string
}

func (u *User) Name() string {
	return fmt.Sprintf("%s %s", u.FirstName, u.LastName)
}

type Customer struct {
	Id       int
	FullName string
}

func (c *Customer) Name() string {
	return c.FullName
}

type Namer interface {
	Name() string
}

func Greet(n Namer) string {
	return fmt.Sprintf("Hello %s", n.Name())
}

func main() {
	u := &User{"Joie", "Lechong"}
	fmt.Println(Greet(u))
	c := &Customer{27, "Francoise"}
	fmt.Println(Greet(c))
}
