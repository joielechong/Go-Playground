// Interfaces are implemented implicitly

// A type implements an interface by implementing its method.
// There is no explicit declartion of intent, no "implements" keyword

package main

import "fmt"

type I interface {
	M()
}

type T struct {
	S string
}

// implements the interface I implicitly
func (t T) M() {
	fmt.Println(t.S)
}

func main() {
	var i I = T{"Hello"}
	i.M()
}
