package main

// One of the msot ubiquitous interfaces is Stringer defined by the ftm package.

// type Stringer interface {
//    String() string
// }
//
// A Stringer is a type that can describe itself as a string. The fmt package(and many others)
// look for this interface to print values.
import "fmt"

type User struct {
	Name string
	Age  int
}

func (p User) String() string {
	return fmt.Sprintf("%v (%v years)\n", p.Name, p.Age)
}

func main() {
	u := User{"Joielechong", 32}
	zoe := User{"Zoe", 11}
	fmt.Println(u, zoe)
}
