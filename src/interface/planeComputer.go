package planecomputer

type PlaneComputer interface {
	Area() float64
	Perimeter() float64
	Volume() float64
}
