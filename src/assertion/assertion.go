package main

import "fmt"

func main() {
	var myInt interface{} = 123

	k, ok := myInt.(int)
	if ok {
		fmt.Println("Succes:", k)
	}

	v, ok := myInt.(float64)
	if ok {
		fmt.Println(v)
	} else {
		fmt.Println("Failed without panicking!")
	}

	i := myInt.(int)
	fmt.Println("No checking:", i)

	j := myInt.(bool)

	// if ok {
	// 	fmt.Println("j : ", j)
	// } else {
	// 	fmt.Println("Not OK")
	// }
	fmt.Println(j)
}
