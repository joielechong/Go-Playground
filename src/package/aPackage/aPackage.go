package aPackage

import "fmt"

func A() {
	fmt.Println("This is function !")
}

func B() {
	fmt.Println("privateConstant:", privateConstant)
}

const MyConstant = 123
const privateConstant = 21
