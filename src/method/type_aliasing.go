package main

import (
	"fmt"
	"strings"
)

type MyString string

func (s MyString) Uppercase() string {
	return strings.ToUpper(string(s))
}

func main() {
	fmt.Println(MyString("test").Uppercase())
}
