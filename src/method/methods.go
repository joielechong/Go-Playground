// A method is a function with a special receiver argument.
// The receiver appears in its own argument list between the func keyword and the method name.

package main

import (
	"fmt"
)

type User struct {
	FirstName, LastName string
}

func (u User) printUser() string {
	return u.FirstName + " " + u.LastName
}

func main() {
	user := User{"Joie", "LeChong"}
	fmt.Println(user.printUser())
}
