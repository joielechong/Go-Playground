package main

import "fmt"

type User struct {
	firstName, lastName string
}

func printUser(u *User) {
	fmt.Println(u.firstName + " " + u.lastName)
}

func (u *User) getDetails() string {
	return u.firstName + " " + u.lastName
}

func main() {
	u := User{"Joie", "Le Chong"}
	printUser(&u)

	fmt.Println(u.getDetails())
}
