package main

import "fmt"

type MyString string

func (s MyString) Print() string {
	return string(s) + " World"
}

func main() {
	f := MyString("Hello")
	fmt.Println(f.Print())
}
