package main

import "fmt"

type User struct {
	firstName, LastName string
}

func printUser(u User) string {
	return u.firstName + " " + u.LastName
}

func main() {
	u := User{"Joie", "Le Chong"}

	fmt.Println(printUser(u))
}
