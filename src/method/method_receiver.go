package main

import "fmt"

type User struct {
	FirstName, LastName string
}

func (u *User) Greeting() string {
	return fmt.Sprintf("Welcome %s %s", u.FirstName, u.LastName)
}

func main() {
	u := &User{"Joie", "Lechong"}

	fmt.Println(u.Greeting())
}
