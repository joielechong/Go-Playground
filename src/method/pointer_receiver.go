package main

import "fmt"

type Rectangle struct {
	width, length int
}

func (rect *Rectangle) calculate() int {
	return rect.length * rect.width
}

func main() {
	rect := Rectangle{5, 4}

	fmt.Printf("Rectangle with width = %d, length = %d\n", rect.width, rect.length)
	fmt.Println("Calculate directly")
	fmt.Println(rect.calculate())

	r := &rect
	fmt.Println("Calculate indirectly")
	fmt.Println(r.calculate())

}
