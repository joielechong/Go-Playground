package main

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type animal struct {
	gorm.Model
	//	ID         int    `gorm:"primary_key;not null;unique;AUTO_INCREMENT"`
	Animaltype string `gorm:"type:TEXT"`
	Nickname   string `gorm:"type:TEXT"`
	Zone       int    `gorm:"type:INTEGER"`
	Age        int    `gorm:"type:INTEGER"`
}

func main() {
	db, err := gorm.Open("sqlite3", "dino.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.DropTableIfExists(&animal{})
	db.AutoMigrate(&animal{})
	db.Table("dinos").CreateTable(&animal{})

	a := animal{
		Animaltype: "Tyrannosaurus rex",
		Nickname:   "rex",
		Zone:       1,
		Age:        11,
	}

	db.Create(&a)
	db.Table("dinos").Create(&a)

	a = animal{
		//		ID:         2,
		Animaltype: "Velociraptor",
		Nickname:   "raptor",
		Zone:       2,
		Age:        15,
	}

	db.Save(&a)

	animals := []animal{}
	db.Debug().Find(&animals, "age > ?", 10)
	fmt.Println(animals)

	db.Debug().Table("animals").Where("nickname = ? and zone = ?", "raptor", 2).Update("age", 16)

}
