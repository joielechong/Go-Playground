package databaselayer

import "errors"

const (
	MYSQL uint8 = iota
	SQLITE
	POSTGRESQL
)

type DinoDBHandler interface {
	GetAvailableDynos([]Animal, error)
	GetDynoByNickname(string) (Animal, error)
	GetdynosByType(string) ([]Animal, error)
	AddAnimal(Animal) error
	UpdateAnimal(Animal, string) error
}

type Animal struct {
	ID         int
	AnimalType string
	Nickname   string
	Zone       int
	Age        int
}

var DBTypeNotSupported = errors.New("The database type provided is not supported...")

func GetDatabaseHandler(dbtype uint8, connection string) (DinoDBHandler, error) {
	switch dbtype {
	case MYSQL:
		return NewMySQLHandler(connection)
	case SQLITE:
		return NewSQLiteHandler(connection)
	case POSTGRESQL:
		return NewPQHandler(connection)
	}
	return nil, DBTypeNotSupported
}
