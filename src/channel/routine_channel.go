package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Start")
	fmt.Println(<-waitAndSend(3, 1))
	fmt.Println("End")
}

// will wait for i seconds before sending value v on the return channel
func waitAndSend(v, i int) chan int {
	retCh := make(chan int)
	go func() {
		time.Sleep(time.Duration(i) * time.Second)
		retCh <- v

	}()
	return retCh
}
