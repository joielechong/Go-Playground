package main

import (
	"fmt"
	"time"
)

func main() {
	ticker := time.NewTicker(700 * time.Millisecond)
	go func() {
		for t := range ticker.C {
			fmt.Println("Tick at", t)
		}
	}()

	time.Sleep(2700 * time.Millisecond)
	ticker.Stop()
}
