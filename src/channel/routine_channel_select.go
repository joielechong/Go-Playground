package main

import (
	"fmt"
	"time"
)

func main() {
	select {
	case v1 := <-waitAndSend(3, 2):
		fmt.Println(v1)
	case v2 := <-waitAndSend(5, 1):
		fmt.Println(v2)
	default:
		fmt.Println("all channels are slow")
	}
}

func waitAndSend(v, i int) chan int {
	retCh := make(chan int)
	go func() {
		time.Sleep(time.Duration(i) * time.Second)
		retCh <- v
	}()
	return retCh
}
