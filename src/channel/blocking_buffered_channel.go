package main

import "fmt"

func main() {
	buffch := make(chan int, 3)
	buffch <- 7 // FIFO
	buffch <- 5

	fmt.Println(<-buffch)
	fmt.Println(<-buffch)
	fmt.Println(<-buffch)

	// Buffered channels block when they are either full or empty
}
