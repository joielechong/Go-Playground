package main

import "fmt"

func sum(s []int, c chan int) {
	total := 0
	for _, v := range s {
		total += v
	}
	c <- total // send total to c
}

func main() {
	s := []int{7, 2, 8, -9, 4, 0}

	channel := make(chan int)
	go sum(s[:len(s)/2], channel)
	go sum(s[len(s)/2:], channel)

	x, y := <-channel, <-channel // receive from c

	fmt.Println(x, y, x+y)
}
