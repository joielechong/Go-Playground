package main

import "fmt"

type User struct {
	id   int
	name string
}

type Player struct {
	User
	playerId int
}

func (u *User) greeting() string {
	return fmt.Sprintf("Hello user %d with name %s", u.id, u.name)
}

func main() {
	player := Player{}
	player.id = 1
	player.name = "Joielechong"
	player.playerId = 2

	fmt.Println(player.greeting())
}
