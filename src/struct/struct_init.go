package main

import "fmt"

type Bootcamp struct {
	Lat float64
	Lng float64
}

func main() {
	x := new(Bootcamp)
	y := &Bootcamp{}
	fmt.Println(*x == *y)
}
