package main

import "fmt"

type User struct {
	id   int
	name string
}

type Player struct {
	User
	playerId int
}

func main() {
	player := Player{}
	player.id = 1
	player.name = "Joielechong"
	player.playerId = 2

	fmt.Printf("%+v", player)
}
