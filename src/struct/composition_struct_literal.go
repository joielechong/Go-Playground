package main

import "fmt"

type User struct {
	id   int
	name string
}

type Player struct {
	User
	playerId int
}

func main() {
	player := Player{
		User{id: 1, name: "Joielechong"},
		2,
	}

	fmt.Printf("%+v", player)
}
