package main

import "fmt"

func main() {
	type XYZ struct {
		X, Y, Z int
	}

	var s1 XYZ
	fmt.Println(s1.Y, s1.Z)

	p1 := XYZ{23, 12, -2}
	p2 := XYZ{Z: 12, Y: 13}
	fmt.Println("p1 = ", p1)
	fmt.Println("p2 = ", p2)

	pSlice := [4]XYZ{}
	fmt.Println("Create slice with = ", pSlice)
	fmt.Println("copy p1 to pSlice[2]")
	pSlice[2] = p1
	fmt.Println("copy p2 to pSlice[0]")
	pSlice[0] = p2
	fmt.Println("now pSlice : ", pSlice)

	p2 = XYZ{1, 2, 3}
	fmt.Println("assign new value to p2 : ", p2)
	fmt.Println("now pSlice : ", pSlice)
}
