package main

import "fmt"

type School struct {
	Name    string
	Address string
}

func main() {
	usu := School{
		"USU",
		"Dr Mansyur, Medan",
	}

	fmt.Println(usu)

	nommensen := School{
		"Nommensen",
		"Jalan Perjuangan",
	}

	fmt.Println(nommensen)

	// change value of struct
	nommensen.Address = "Medan baru"

	fmt.Println(nommensen)
}
