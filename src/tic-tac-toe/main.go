package main

import (
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"log"
	"os"
	"runtime"
	"time"
	"unsafe"

	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/golang-ui/nuklear/nk"
	"github.com/xlab/closer"
)

const (
	winWidth  = 500
	winHeight = 500

	maxVertexBuffer  = 512 * 1024
	maxElementBuffer = 128 * 1024
	assetCircle      = "assets/o.png"
	assetCross       = "assets/x.png"
	assetRect        = "assets/rect.png"
)

func init() {
	runtime.LockOSThread()
}

var imageMaps map[string]nk.Image
var selectedPosition [9]nk.Image

func main() {
	if err := glfw.Init(); err != nil {
		closer.Fatalln(err)
	}
	glfw.WindowHint(glfw.ContextVersionMajor, 3)
	glfw.WindowHint(glfw.ContextVersionMinor, 2)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	win, err := glfw.CreateWindow(winWidth, winHeight, "Tic Tac Toe", nil, nil)
	if err != nil {
		closer.Fatalln(err)
	}
	win.MakeContextCurrent()

	width, height := win.GetSize()
	log.Printf("glfw: created window %dx%d", width, height)

	if err := gl.Init(); err != nil {
		closer.Fatalln("opengl: init failed:", err)
	}
	gl.Viewport(0, 0, int32(width), int32(height))

	ctx := nk.NkPlatformInit(win, nk.PlatformInstallCallbacks)

	atlas := nk.NewFontAtlas()
	nk.NkFontStashBegin(&atlas)
	sansFont := nk.NkFontAtlasAddFromBytes(atlas, MustAsset("assets/FreeSans.ttf"), 16, nil)

	nk.NkFontStashEnd()

	if sansFont != nil {
		nk.NkStyleSetFont(ctx, sansFont.Handle())
	}

	imageMaps = loadImages(assetCircle, assetCross, assetRect)
	fmt.Println("imageMaps = ", imageMaps)
	for i := 0; i < 9; i++ {
		selectedPosition[i] = imageMaps[assetRect]
	}
	fmt.Println("imageMaps[assetRect] = ", imageMaps[assetRect])

	exitC := make(chan struct{}, 1)
	doneC := make(chan struct{}, 1)
	closer.Bind(func() {
		close(exitC)
		<-doneC
	})

	state := &State{
		bgColor: nk.NkRgba(28, 48, 62, 255),
	}
	fpsTicker := time.NewTicker(time.Second / 30)
	for {
		select {
		case <-exitC:
			nk.NkPlatformShutdown()
			glfw.Terminate()
			fpsTicker.Stop()
			close(doneC)
			return
		case <-fpsTicker.C:
			if win.ShouldClose() {
				close(exitC)
				continue
			}
			glfw.PollEvents()
			gfxMain(win, ctx, state)
		}
	}
}

func gfxMain(win *glfw.Window, ctx *nk.Context, state *State) {

	nk.NkPlatformNewFrame()

	// Layout
	bounds := nk.NkRect(0, 0, 500, 500)
	update := nk.NkBegin(ctx, "", bounds, nk.WindowBorder|nk.WindowTitle)
	nk.SetBackgroundColor(ctx, nk.NkRgb(0, 120, 215))
	if update > 0 {
		nk.NkLayoutRowStatic(ctx, 140, 140, 3)
		{
			for i := 0; i < 9; i++ {

				if nk.NkButtonImage(ctx, selectedPosition[i]) > 0 {
					fmt.Println("selected = ", selectedPosition[i])
					fmt.Println(selectedPosition[i] == imageMaps[assetRect])
					isRect := selectedPosition[i] == imageMaps[assetRect]
					if isRect {
						selectedPosition[i] = imageMaps[assetCircle]
					} else {
						selectedPosition[i] = imageMaps[assetCircle]
					}
					fmt.Println("button id = ", i)

				}

			}
		}

	}
	nk.NkEnd(ctx)

	// Render
	bg := make([]float32, 4)
	nk.NkColorFv(bg, state.bgColor)
	width, height := win.GetSize()
	gl.Viewport(0, 0, int32(width), int32(height))
	gl.Clear(gl.COLOR_BUFFER_BIT)
	gl.ClearColor(bg[0], bg[1], bg[2], bg[3])
	nk.NkPlatformRender(nk.AntiAliasingOn, maxVertexBuffer, maxElementBuffer)
	win.SwapBuffers()
}

type State struct {
	bgColor nk.Color
}

var Selected = []int32{0, 0, 0, 0, 0, 0, 1, 1, 1}
var Background [9]nk.Color

func onError(code int32, msg string) {
	log.Printf("[glfw ERR]: error %d: %s", code, msg)
}

func rgbaTex(tex *uint32, rgba *image.RGBA) nk.Image {
	if tex == nil {
		gl.GenTextures(1, tex)
	}
	gl.BindTexture(gl.TEXTURE_2D, *tex)
	gl.TexParameterf(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST)
	gl.TexParameterf(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR_MIPMAP_NEAREST)
	gl.TexParameterf(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameterf(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA8, int32(rgba.Bounds().Dx()), int32(rgba.Bounds().Dy()),
		0, gl.RGBA, gl.UNSIGNED_BYTE, unsafe.Pointer(&rgba.Pix[0]))
	gl.GenerateMipmap(gl.TEXTURE_2D)
	return nk.NkImageId(int32(*tex))
}

func imgRGBA(img image.Image) *image.RGBA {
	switch trueim := img.(type) {
	case *image.RGBA:
		return trueim
	default:
		copy := image.NewRGBA(trueim.Bounds())
		draw.Draw(copy, trueim.Bounds(), trueim, image.Pt(0, 0), draw.Src)
		return copy
	}
}

func pngRGBA(path string) *image.RGBA {
	f, err := os.Open(path)
	if err != nil {
		return nil
	}
	defer f.Close()
	img, err := png.Decode(f)
	if err != nil {
		return nil
	}
	return imgRGBA(img)
}

func loadImages(pngs ...string) map[string]nk.Image {
	imgMap := make(map[string]nk.Image, len(pngs))
	gl.Enable(gl.TEXTURE_2D)
	teks := make([]uint32, len(pngs))
	for i := 0; i < len(pngs); i++ {
		teks[i] = uint32(i)
	}
	count := 0
	for _, path := range pngs {
		//		var tex uint32
		if img := pngRGBA(path); img != nil {
			nkImage := rgbaTex(&teks[count], img)
			fmt.Println("nkImage:", nkImage)
			imgMap[path] = nkImage
		} else {
			log.Println("[WARN] failed to load", path)
		}
		count++
	}
	return imgMap
}
