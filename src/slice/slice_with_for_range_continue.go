package main

import "fmt"

func main() {
	pow := make([]int, 10)

	for idx, _ := range pow {
		pow[idx] = 1 << uint(idx) // 2**i
	}

	fmt.Println(pow)
}
