package main

import "fmt"

func main() {
	a8 := []int{-10, 1, 2, 3, 4, 5, 6, 7}
	a5 := []int{-1, -2, -3, -4, -5}
	fmt.Println("a8:", a8)
	fmt.Println("a5:", a5)

	copy(a8, a5)
	fmt.Println("a8:", a8)
	fmt.Println("a5:", a5)
	fmt.Println()

	b6 := []int{-10, 1, 2, 3, 4, 5}
	b4 := []int{-1, -2, -3, -4}
	fmt.Println("b6:", b6)
	fmt.Println("b4:", b4)
	copy(b4, b6)
	fmt.Println("b6:", b6)
	fmt.Println("b4:", b4)
	fmt.Println()
	array4 := [4]int{4, -4, 4, -4}
	s6 := []int{1, 1, -1, -1, 5, -5}
	copy(s6, array4[0:])
	fmt.Println("array4:", array4[0:])
	fmt.Println("s6:", s6)
	fmt.Println()

	array5 := [5]int{5, -5, 5, -5, 5}
	s7 := []int{7, -7, 7, -7, 7, -7, 7}
	copy(array5[0:], s7)
	fmt.Println("array5:", array5)
	fmt.Println("s7:", s7)

}
