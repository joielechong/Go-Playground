package main

import "fmt"

func main() {
	var slices []int

	printSlice(slices)

	// append works on nil slices
	slices = append(slices, 0)
	printSlice(slices)

	//growing the slices by one
	slices = append(slices, 7)
	printSlice(slices)

	slices = append(slices, 1, 2, 3, 4)
	printSlice(slices)
}

func printSlice(s []int) {
	fmt.Printf("length = %d, capacity = %d, %v\n", len(s), cap(s), s)
}
