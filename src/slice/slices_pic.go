package main

import "golang.org/x/tour/pic"

/**
Showing a bluescale image. The image can only
seen in Go Playground
**/
func Pic(dx, dy int) [][]uint8 {
	slices := make([][]uint8, dy)

	for y := 0; y < dy; y++ {
		sliceX := make([]uint8, dx)
		for x := 0; x < dx; x++ {
			sliceX[x] = uint8(y) * uint8(x)
		}
		slices[y] = sliceX
	}
	return slices
}

func main() {
	pic.Show(Pic)
}
