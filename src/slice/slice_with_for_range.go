package main

import "fmt"

func main() {
	var slices []int

	slices = append(slices, 1, 2, 3, 4, 5, 6)

	for idx, val := range slices {
		fmt.Printf("idx %d, val %d\n", idx, val)
	}
}
