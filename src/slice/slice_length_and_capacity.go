package main

import "fmt"

func main() {

	s := []int{2, 3, 5, 7, 11, 13}

	printSlice(s)

	// Drop its first two values.
	s = s[2:]
	printSlice(s)
}

func printSlice(s []int) {
	fmt.Printf("length = %d capacity = %d %v\n", len(s), cap(s), s)
}
