package main

import "fmt"

func main() {
	var slice []int

	fmt.Printf("slice length %d, slice capacity %d\n", len(slice), cap(slice))

	if slice == nil {
		fmt.Println("slice is nil")
	}
}
