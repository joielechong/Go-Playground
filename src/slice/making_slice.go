package main

import "fmt"

func main() {
	cities := make([]string, 4)
	cities[0] = "Medan"
	cities[1] = "Siantar"
	cities[2] = "Besitang"
	cities[3] = "Tarutung"

	fmt.Printf("%q", cities)
}
