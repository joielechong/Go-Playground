package main

import "fmt"

func main() {
	s := make([]string, 3)
	fmt.Println("s:", s)

	s[0] = "a"
	s[1] = "b"
	s[2] = "c"
	fmt.Println("set s: ", s)
	fmt.Println("s[2] : ", s[2])
	fmt.Println("len s:", len(s))

	fmt.Println("appending \"d\"")
	s = append(s, "d")
	fmt.Println("appendding \"e\" and \"f\"")
	s = append(s, "e", "f")
	fmt.Println("set s: ", s)

	fmt.Println("Create another slice, t")

	t := make([]string, len(s))
	copy(t, s)
	fmt.Println("copy s to t:", t)

	fmt.Println("get a slice of t with t[2:5]")

	u := t[2:5]
	fmt.Println("t[2:5]:", u)

	u = t[:5]
	fmt.Println("t[:5]:", u)

	u = t[2:]
	fmt.Println("t[2:]:", u)
}
