package main

import "fmt"

func main() {
	countries := []string{"Besitang", "Medan", "Tarutung"}

	smallCountries := []string{"Jakarta", "Bandung", "Surabaya"}

	countries = append(countries, smallCountries...)

	fmt.Printf("%q", countries)
}
