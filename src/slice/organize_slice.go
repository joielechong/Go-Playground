package main

import "fmt"

var names = []string{"Katrina", "Evan", "Neil", "Adam", "Martin", "Matt",
	"Emma", "Isabella", "Emily", "Madison",
	"Ava", "Olivia", "Sophia", "Abigail",
	"Elizabeth", "Chloe", "Samantha",
	"Addison", "Natalie", "Mia", "Alexis"}

func main() {
	// first, sort the name
	for i := 0; i < len(names)-1; i++ {
		for j := 0; j < len(names)-1-i; j++ {
			if len(names[j]) > len(names[j+1]) {
				swap(&names[j], &names[j+1])
			}
		}
	}

	fmt.Println(names)

	// then we get the minimum and maximum length of string
	min := len(names[0])
	max := len(names[len(names)-1])

	fmt.Println(min, max)

	// now make the group of string based on string length
	slices := make([][]string, max)

	for _, name := range names {
		slices[len(name)-1] = append(slices[len(name)-1], name)
	}

	fmt.Println(slices)

}

func swap(first *string, second *string) {
	temp := *first
	*first = *second
	*second = temp
}
