package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

var passwords = []string{
	"1Ki77y", ".Susan53", "jelly22fish", "sm3llycat",
	"ebay.44", "!LoveMyPiano", "BankLogin13", "!usher",
	"deltagamm@", "dOG.lov3r", "sn00pdoggydOG", "fac3b00k3r",
	"molijk3", "goedgebruigt", "mevr0u3w", "w33r0m!",
}

func random(min, max int) int {
	return rand.Intn(max-min) + min
}

func main() {
	MIN := 0
	MAX := 94
	SEED := time.Now().Unix()

	var LENGTH int64 = 8

	arguments := os.Args

	switch len(arguments) {
	case 2:
		LENGTH, _ = strconv.ParseInt(os.Args[1], 10, 64)
	default:
		fmt.Println("Using default values!")
	}

	var initialPass = getRandomInitialPassword(SEED)
	var randString = randomString(MIN, MAX, LENGTH, SEED)

	fmt.Println(initialPass + randString)
}

func getRandomInitialPassword(seed int64) string {
	rand.Seed(seed)
	myRand := random(0, len(passwords)-1)

	return passwords[myRand]
}

func randomString(min int, max int, length int64, seed int64) string {
	startChar := "!"
	rand.Seed(seed)
	var i int64 = 1
	var result string
	for {
		myRand := random(min, max)
		result += string(startChar[0] + byte(myRand))

		if i == length {
			break
		}
		i++
	}

	return result
}
