package main

import "fmt"

func main() {
	i, j := 50, 51

	var x = 10

	// The & operator generate a pointer to its operand.
	p := &i

	// The * operator denotes the pointer's underlaying value.
	fmt.Println(*p)

	// set value true through the pointer.
	// this is know as dereferencing or indirecting
	*p = 11
	fmt.Println(*p)

	p = &i // point to i
	*p = *p / 25
	fmt.Println(i)

	p = &j
	*p = *p - 1
	fmt.Println(*p)

	y := &x
	fmt.Println(*y)

}
