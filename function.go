package main

import "fmt"

func square(value int) {
	fmt.Println(value * value)
}

func total(a int, b int) {
	fmt.Println(a + b)
}

// function with a return
func substract(a int, b int) int {
	return a - b
}

// when two or more consecutive named function parameters share a type,
// we can omit the type from all but the last.
func sum(a, b, c, d int) int {
	return a + b + c + d
}

func main() {
	square(20)
	total(5, 10)
	fmt.Println(substract(10, 5))
	fmt.Println("sum = ", sum(1, 2, 3, 4))
}
