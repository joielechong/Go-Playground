package main

import "fmt"

// go has only one looping construct, the for loop
func main() {
	count := 0
	for i := 0; i < 10; i++ {
		count += i
	}
	fmt.Println(count)

	// for without init and post statement
	sum := 1
	for sum < 100 {
		sum += sum
	}

	fmt.Println(sum)
}
