package main

import (
	"bufio"
	"fmt"
	"strings"
)

func main() {
	s := "I felt so good like anything was possible\n" +
		"I hit cruise control and rubbed my eyes\n" +
		"The last three days the rain was un-stoppable\n" +
		"It was always cold, no sunshine\n" +
		"I rolled on as the sky grew dark\n" +
		"I put the pedal down to make some time\n"

	scanner := bufio.NewScanner(strings.NewReader(s))
	scanner.Split(bufio.ScanWords)

	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(line)
	}
}
