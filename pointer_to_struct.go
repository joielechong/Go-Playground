package main

import "fmt"

type User struct {
	Name  string
	Email string
}

func main() {

	joie := User{
		"Joie",
		"joie@example.com",
	}

	p := &joie

	fmt.Println("struct = ", joie)
	fmt.Println("pointer p.Email = ", p.Email)

	fmt.Println("Change struct value via pointer")
	p.Email = "test@example.com"
	fmt.Println("p.Email = ", p.Email)
	fmt.Println("struct now = ", joie)

}
