package main

import "fmt"

func summary(text string) {
	defer fmt.Println("The summary")
	fmt.Println(text)
	fmt.Println("Good story!")
}

func main() {
	content := "Once upon a time there is a little cat named Whitey"
	defer fmt.Println("Create the summary")
	summary(content)
}
