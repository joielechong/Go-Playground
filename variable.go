package main

import "fmt"

var localVariable string = "Local variable"

func main() {
	// declare variable
	var a = "a variable"

	fmt.Println(a)

	// create and initialize variable
	b := 10

	fmt.Println(b)

	var d, e int = 3, 4
	fmt.Println(d, e)

	f, g := 5, 6
	fmt.Println(f, g)

	// string default to empty
	var h string
	fmt.Println(h)

	fmt.Println(localVariable)
}
